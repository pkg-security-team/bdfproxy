% BDF_PROXY(1) Bdf_proxy Man Page
% Philippe Thierry
% June 2017
# NAME
bdf_proxy - Patch binaries during download ala MITM

# SYNOPSIS
**bdf_proxy**

The configuration file is deployed in /etc/bdfproxy/bdfproxy.cfg.
When starting the tool, it read its configuration from:
  - a bdfproxy.cfg file in the current dir, if it exists
  - /etc/bdfproxy/bdfproxy.cfg if the previous file does not exist

# DESCRIPTION

This tool is for security professionals and researchers only.

Lan usage:

```<Internet>----<mitmMachine>----<userLan>```

Wifi usage:

```<Internet>----<mitmMachine>----<wifiPineapple>```

The tool is based on a configuration file, named bdfproxy.cfg, installed in
/etc/bdfproxy directory.

# SEE ALSO

backdoor-factory(1)

# HISTORY
May 2017, Originally compiled by Philippe Thierry (phil at reseau-libre dot com)
